/*
Create a `withStorage` higher order component that manages saving and retrieving
the `sidebarIsOpen` state to local storage
*/

import "./index.css";
import React from "react";
import MenuIcon from "react-icons/lib/md/menu";
import { set, get, subscribe } from "./local-storage";

const withStorage = (stateProp = "sidebarIsOpen", initValue = true) => Comp => {
  return class WithStorage extends React.Component {
    state = {
      [stateProp]: get(stateProp, initValue)
    };

    componentDidMount() {
      this.unsubscribe = subscribe(() => {
        this.setState({
          [stateProp]: get(stateProp)
        });
      });
    }

    componentWillUnmount() {
      this.unsubscribe();
    }

    render() {
      return <Comp {...this.state} {...this.props} />;
    }
  };
};

class App extends React.Component {
  render() {
    const { sidebarIsOpen } = this.props;
    return (
      <div className="app">
        <header>
          <button
            className="sidebar-toggle"
            title="Toggle menu"
            onClick={() => {
              set("sidebarIsOpen", !sidebarIsOpen);
            }}
          >
            <MenuIcon />
          </button>
        </header>

        <div className="container">
          <aside className={sidebarIsOpen ? "open" : "closed"} />
          <main />
        </div>
      </div>
    );
  }
}

export default withStorage("sidebarIsOpen", true)(App);
