////////////////////////////////////////////////////////////////////////////////
import React from "react";
import { createBrowserHistory } from "history";
import * as PropTypes from "prop-types";

/*
// create a new history instance
history = createBrowserHistory()

// read the current URL
history.location

// listen for changes to the URL
const unsubscribe = history.listen(() => {
  history.location // is now different
})

// change the URL
history.push('/something')
*/

class Router extends React.Component {
  _history = createBrowserHistory();
  unsubscribe = this._history.listen(() => {
    this.locationChangeHandler();
  });

  static childContextTypes = {
    _history: PropTypes.object.isRequired,
    currnetRoute: PropTypes.string.isRequired
  };

  state = {
    currnetRoute: this._history.location.pathname || "/"
  };

  componentDidMount() {
    if (this.state.currnetRoute !== this._history.location.pathname) {
      this.setState({ currnetRoute: this._history.location.pathname });
    }
  }
  componentWillUnmount() {
    this.unsubscribe();
  }

  getChildContext() {
    return {
      _history: this._history,
      currnetRoute: this.state.currnetRoute
    };
  }

  locationChangeHandler = () => {
    this.setState({ currnetRoute: this._history.location.pathname });
  };

  render() {
    return this.props.children;
  }
}

class Route extends React.Component {
  static contextTypes = {
    _history: PropTypes.object.isRequired,
    currnetRoute: PropTypes.string.isRequired
  };

  renderComponetHandler = () => {
    const { path, component: Component, exact, render } = this.props;
    const { _history } = this.context;

    if (
      exact &&
      _history.location.pathname === path &&
      typeof render === "function"
    ) {
      return render();
    }
    if (
      typeof Component === "function" &&
      _history.location.pathname === path
    ) {
      // return component({ ...this.props });
      return <Component />;
    }
    return null;
  };

  render() {
    return this.renderComponetHandler();
  }
}

class Link extends React.Component {
  static contextTypes = {
    _history: PropTypes.object.isRequired
  };

  handleClick = e => {
    e.preventDefault();
    this.context._history.push(this.props.to);
  };

  render() {
    return (
      <a href={`${this.props.to}`} onClick={this.handleClick}>
        {this.props.children}
      </a>
    );
  }
}

export { Router, Route, Link };
