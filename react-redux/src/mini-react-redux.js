import React, { Component } from "react";
import PropTypes from "prop-types";

/*
// Tips:

// Get the store's state
store.getState()

// Dispatch changes to the store
// (you won't need to call this but you'll pass it to mapDispatchToProps)
store.dispatch(action)

// subscribe to changes to the store
store.subscribe(() => {})

// unsubscribe from the store
unsubscribe = store.subscribe(() => {})
unsubscribe()
*/

class Provider extends React.Component {
  static childContextTypes = {
    store: PropTypes.object.isRequired
  };

  getChildContext() {
    return {
      store: this.props.store
    };
  }
  render() {
    return this.props.children;
  }
}

const connect = (mapStateToProps, mapDispatchToProps) => Comp => {
  return class extends Component {
    static contextTypes = {
      store: PropTypes.object.isRequired
    };

    componentDidMount() {
      this.unsubscribe = this.context.store.subscribe(() => {
        //here we will have new store
        // console.log(this.context.store.getState());
        this.forceUpdate();
      });
    }

    componentWillUnmount() {
      this.unsubscribe();
    }

    returnPropsHandler = () => {
      const { store } = this.context;
      return {
        ...this.props,
        ...mapStateToProps(store.getState()),
        ...mapDispatchToProps(store.dispatch)
      };
    };
    render() {
      return <Comp {...this.returnPropsHandler()} />;
    }
  };
};

export { Provider, connect };
