/*
- Make the Play button work
- Make the Pause button work
- Disable the play button if it's playing
- Disable the pause button if it's not playing
- Make the PlayPause button work
- Make the JumpForward button work
- Make the JumpBack button work
- Make the progress bar work
  - change the width of the inner element to the percentage of the played track
  - add a click handler on the progress bar to jump to the clicked spot

Here is the audio API you'll need to use, `audio` is the <audio/> dom nod
instance, you can access it as `this.audio` in `AudioPlayer`

```js
// play/pause
audio.play()
audio.pause()

// change the current time
audio.currentTime = audio.currentTime + 10
audio.currentTime = audio.currentTime - 30

// know the duration
audio.duration

// values to calculate relative mouse click position
// on the progress bar
event.clientX // left position *from window* of mouse click
const rect = node.getBoundingClientRect()
rect.left // left position *of node from window*
rect.width // width of node
```

Other notes about the `<audio/>` tag:

- You can't know the duration until `onLoadedData`
- `onTimeUpdate` is fired when the currentTime changes
- `onEnded` is called when the track plays through to the end and is no
  longer playing

Good luck!
*/

import "./index.css";
import React from "react";
import * as PropTypes from "prop-types";
import podcast from "./podcast.mp3";
import mario from "./mariobros.mp3";
import FaPause from "react-icons/lib/fa/pause";
import FaPlay from "react-icons/lib/fa/play";
import FaRepeat from "react-icons/lib/fa/repeat";
import FaRotateLeft from "react-icons/lib/fa/rotate-left";

class AudioPlayer extends React.Component {
  static childContextTypes = {
    onPlay: PropTypes.func.isRequired,
    onPause: PropTypes.func.isRequired,
    isPlaying: PropTypes.bool.isRequired,
    onPlaySkip: PropTypes.func.isRequired,
    onPlayBack: PropTypes.func.isRequired,
    onCurrentTimeChange: PropTypes.func.isRequired,
    duration: PropTypes.number.isRequired,
    onTimeUpdate: PropTypes.func.isRequired,
    currentTime: PropTypes.number.isRequired
  };

  state = {
    isPlaying: false,
    duration: 0,
    currentTime: 0
  };

  getChildContext() {
    return {
      onPlay: this.onPlay,
      onPause: this.onPause,
      isPlaying: this.state.isPlaying,
      onPlayBack: this.onPlayBack,
      onPlaySkip: this.onPlaySkip,
      duration: this.state.duration,
      onCurrentTimeChange: this.onCurrentTimeChange,
      onTimeUpdate: this.onTimeUpdate,
      currentTime: this.state.currentTime
    };
  }

  setAudio = e => (this.audio = e);

  onPlay = e => {
    this.setState({ isPlaying: true }, () => this.audio.play());
  };
  onPause = e => {
    this.setState({ isPlaying: false }, () => this.audio.pause());
  };
  onPlaySkip = e => {
    this.audio.currentTime = this.audio.currentTime + 10;
  };
  onPlayBack = e => {
    this.audio.currentTime = this.audio.currentTime - 10;
  };

  onAudioLoad = e => {
    this.setState({ duration: e.target.duration });
  };

  onCurrentTimeChange = percentage => {
    const remainingPers = 100 - parseInt(percentage);
    const remainingTime = (remainingPers * this.state.duration) / 100;
    const currentTime = this.state.duration - remainingTime;
    this.audio.currentTime = currentTime;
  };

  onTimeUpdate = e => {
    if (e.target.currentTime - this.state.currentTime < 1) return null;
    this.setState({ currentTime: e.target.currentTime });
  };

  render() {
    const { source } = this.props;
    return (
      <div className="audio-player">
        <audio
          src={source}
          onTimeUpdate={this.onTimeUpdate}
          onLoadedData={this.onAudioLoad}
          onEnded={null}
          ref={e => this.setAudio(e)}
        />
        {this.props.children}
      </div>
    );
  }
}

class Play extends React.Component {
  static contextTypes = {
    onPlay: PropTypes.func.isRequired,
    isPlaying: PropTypes.bool.isRequired
  };

  render() {
    const { isPlaying, onPlay } = this.context;
    return (
      <button
        className="icon-button"
        onClick={onPlay}
        disabled={isPlaying}
        title="play"
      >
        <FaPlay />
      </button>
    );
  }
}

class Pause extends React.Component {
  static contextTypes = {
    onPause: PropTypes.func.isRequired,
    isPlaying: PropTypes.bool.isRequired
  };
  render() {
    const { isPlaying, onPause } = this.context;
    return (
      <button
        className="icon-button"
        onClick={onPause}
        disabled={!isPlaying}
        title="pause"
      >
        <FaPause />
      </button>
    );
  }
}

class PlayPause extends React.Component {
  static contextTypes = {
    onPlay: PropTypes.func.isRequired,
    onPause: PropTypes.func.isRequired,
    isPlaying: PropTypes.bool.isRequired
  };
  render() {
    const { isPlaying } = this.context;
    return !isPlaying ? <Play /> : <Pause />;
  }
}

class JumpForward extends React.Component {
  static contextTypes = {
    isPlaying: PropTypes.bool.isRequired,
    onPlaySkip: PropTypes.func.isRequired
  };

  render() {
    const { isPlaying, onPlaySkip } = this.context;
    return (
      <button
        className="icon-button"
        onClick={onPlaySkip}
        disabled={!isPlaying}
        title="Forward 10 Seconds"
      >
        <FaRepeat />
      </button>
    );
  }
}

class JumpBack extends React.Component {
  static contextTypes = {
    isPlaying: PropTypes.bool.isRequired,
    onPlayBack: PropTypes.func.isRequired
  };
  render() {
    const { isPlaying, onPlayBack } = this.context;
    return (
      <button
        className="icon-button"
        onClick={onPlayBack}
        disabled={!isPlaying}
        title="Back 10 Seconds"
      >
        <FaRotateLeft />
      </button>
    );
  }
}

class Progress extends React.Component {
  static contextTypes = {
    isPlaying: PropTypes.bool.isRequired,
    onPlayBack: PropTypes.func.isRequired,
    onCurrentTimeChange: PropTypes.func.isRequired,
    onTimeUpdate: PropTypes.func.isRequired,
    currentTime: PropTypes.number.isRequired,
    duration: PropTypes.number.isRequired
  };

  componentDidUpdate(prevProps) {
    this.updateProgress();
  }

  onProgresClick = e => {
    const rect = document.querySelector(".progress").getBoundingClientRect();
    const newWidth = `${((e.clientX - rect.left) * 100) / rect.width}%`;
    this.progress.style.width = newWidth;
    this.context.onCurrentTimeChange(newWidth);
  };

  updateProgress = () => {
    const { currentTime, duration } = this.context;
    const newWidth = `${(currentTime * 100) / duration}%`;
    this.progress.style.width = newWidth;
  };

  render() {
    return (
      <div className="progress" onClick={this.onProgresClick}>
        <div
          ref={e => (this.progress = e)}
          style={{ width: 0 }}
          className="progress-bar"
        />
      </div>
    );
  }
}

const Exercise = () => (
  <div className="exercise">
    <AudioPlayer source={mario}>
      <Play /> <Pause /> <span className="player-text">Mario Bros. Remix</span>
      <Progress />
    </AudioPlayer>

    <AudioPlayer source={podcast}>
      <PlayPause /> <JumpBack /> <JumpForward />{" "}
      <span className="player-text">
        React30 Episode 010: React Virtualized
      </span>
      <Progress />
    </AudioPlayer>
  </div>
);

export default Exercise;
